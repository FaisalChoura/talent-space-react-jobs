import React from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import "./App.css";
import ApolloClient from "./apollo";
import Filters from "./components/Filters";
import "antd/dist/antd.css";
import { Row, Col } from "antd";
import { FiltersProvider } from "./contexts/FiltersContext";
import JobList from "./components/JobList";

function App() {
  return (
    <ApolloProvider client={ApolloClient}>
      <div className="App">
        <Row>
          <Col span={16} offset={4}>
            <FiltersProvider>
              <Filters />
              <Row gutter={8} style={{ height: "69vh", overflow: "auto" }}>
                <JobList />
              </Row>
            </FiltersProvider>
          </Col>
        </Row>
      </div>
    </ApolloProvider>
  );
}

export default App;
