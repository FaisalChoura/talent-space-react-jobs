import React, { useState, useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { QUERY_ALL_CITIES } from "../queries/Cities";
import { QUERY_ALL_COMPANIES } from "../queries/Companies";
import { QUERY_ALL_INVESTORSS } from "../queries/Investors";
import { Row, Col, Select, Input, Button } from "antd";
import Loading from "./Loading";
import { FiltersContext } from "../contexts/FiltersContext";

const { Search } = Input;
const { Option } = Select;

const Filters = (props) => {
  const {
    loading: loading_cities,
    error: cities_error,
    data: data_job_cities,
  } = useQuery(QUERY_ALL_CITIES);
  const {
    loading: loading_comapnies,
    error: companies_error,
    data: data_companies,
  } = useQuery(QUERY_ALL_COMPANIES);
  const {
    loading: loading_investors,
    error: investors_error,
    data: data_investors,
  } = useQuery(QUERY_ALL_INVESTORSS);

  const {
    filteredTitle,
    selectedCity,
    selectedCompanyId,
    selectedInvestorIds,
  } = useContext(FiltersContext);
  const [title, setTitle] = filteredTitle;
  const [city, setCity] = selectedCity;
  const [companyId, setCompanyId] = selectedCompanyId;
  const [investorIds, setInvestorIds] = selectedInvestorIds;
  const [searchField, setSearchField] = useState("");

  if (loading_cities || loading_comapnies || loading_investors)
    return <Loading />;
  if (companies_error || investors_error || cities_error) return <p>Error</p>;

  const onTitleSearch = (value) => {
    setTitle(value);
  };
  const onCityChange = (value) => {
    setCity(value);
  };
  const onCompanyChange = (value) => {
    setCompanyId(value);
  };
  const onInvestorsSelect = (value) => {
    setInvestorIds(value);
  };
  const clearFliters = (e) => {
    e.preventDefault();
    setCity(undefined);
    setCompanyId(undefined);
    setTitle(undefined);
    setInvestorIds(undefined);
    setSearchField(undefined);
  };

  const listOfCities = () => {
    return data_job_cities.jobs.map(({ city }) => (
      <Option key={city} value={city}>
        {city}
      </Option>
    ));
  };
  const listOfCompanies = () => {
    return data_companies.companies.map(({ id, name }) => (
      <Option key={id} value={id}>
        {name}
      </Option>
    ));
  };
  const listOfInvestors = () => {
    return data_investors.investors.map(({ id, name }) => (
      <Option key={id} value={name}>
        {name}
      </Option>
    ));
  };

  return (
    <div>
      <Row
        style={{
          backgroundColor: "#FCFCFC",
          padding: 16,
          marginBottom: 8,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          minHeight: "30vh",
          boxShadow: "0px 5px 8px #D8D8D8",
        }}
      >
        <Col span={16} offset={4}>
          <h1 style={{ color: "#2c3e50" }}>Job Search</h1>

          <form>
            <Search
              placeholder="Search by job title"
              onSearch={onTitleSearch}
              onChange={(e) => setSearchField(e.target.value)}
              value={searchField}
              style={{ width: "100%", marginBottom: 8 }}
            />
            <Select
              value={city}
              style={{ width: "50%", marginBottom: 8 }}
              placeholder="Select City"
              onChange={onCityChange}
            >
              {listOfCities()}
            </Select>
            <Select
              placeholder="Select Comapny"
              value={companyId}
              style={{ width: "50%", marginBottom: 8 }}
              onChange={onCompanyChange}
            >
              {listOfCompanies()}
            </Select>
            <Select
              mode="multiple"
              style={{ width: "100%", marginBottom: 8 }}
              value={investorIds}
              placeholder="Select Investors"
              onChange={onInvestorsSelect}
            >
              {listOfInvestors()}
            </Select>
            <Button block danger onClick={clearFliters}>
              Clear Filters
            </Button>
          </form>
        </Col>
      </Row>
    </div>
  );
};

export default Filters;
