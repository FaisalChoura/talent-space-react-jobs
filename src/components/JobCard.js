import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBuilding,
  faUsers,
  faMapMarkerAlt,
} from "@fortawesome/free-solid-svg-icons";
import { Row, Col, Card, Tag } from "antd";
import PropTypes from "prop-types";

const JobCard = (props) => {
  const investorList = (investors) => {
    return investors.map((company_investor) => (
      <Tag color="blue">{company_investor.investor.name}</Tag>
    ));
  };
  return (
    <Col span={12}>
      <Card
        size="small"
        title={props.title}
        bordered={true}
        style={{ marginBottom: 8 }}
      >
        <Row>
          <Col span={12}>
            <p>
              <FontAwesomeIcon
                style={{ marginRight: "4px" }}
                icon={faBuilding}
              ></FontAwesomeIcon>
              Company: {props.company.name}
            </p>
          </Col>
          <Col span={12}>
            <p>
              <FontAwesomeIcon
                style={{ marginRight: "4px" }}
                icon={faMapMarkerAlt}
              ></FontAwesomeIcon>
              City: {props.city}
            </p>
          </Col>
        </Row>
        <Row>
          <p>
            <FontAwesomeIcon
              style={{ marginRight: "4px" }}
              icon={faUsers}
            ></FontAwesomeIcon>
            Investors: {investorList(props.company.company_investors)}
          </p>
        </Row>
      </Card>
    </Col>
  );
};

JobCard.propTypes = {
  id: PropTypes.number,
  company: PropTypes.object,
  title: PropTypes.string,
  city: PropTypes.string,
};

export default JobCard;
