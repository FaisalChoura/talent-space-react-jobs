import React, { useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { QUERY } from "../queries/Jobs";
import Loading from "./Loading";
import { FiltersContext } from "../contexts/FiltersContext";
import JobCard from "./JobCard";

const JobList = (props) => {
  const {
    filteredTitle,
    selectedCity,
    selectedCompanyId,
    selectedInvestorIds,
  } = useContext(FiltersContext);
  const [title] = filteredTitle;
  const [city] = selectedCity;
  const [companyId] = selectedCompanyId;
  const [investorIds] = selectedInvestorIds;

  const { loading, error, data } = useQuery(QUERY, {
    variables: {
      company_id: companyId,
      title: !title ? null : `%${title}%`,
      investor_ids:
        investorIds && investorIds.length !== 0 ? investorIds : null,
      city_name: city,
    },
  });

  if (loading) return <Loading />;
  if (error) return <p>Error</p>;

  return data.jobs.map(({ id, title, city, company }) => (
    <JobCard
      key={id}
      id={id}
      title={title}
      city={city}
      company={company}
    ></JobCard>
  ));
};

export default JobList;
