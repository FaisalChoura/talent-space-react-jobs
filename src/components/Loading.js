import React from "react";
import { Spin, Row } from "antd";

const Loading = (props) => {
  return (
    <Row
      style={{
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Spin size="large" />
    </Row>
  );
};

export default Loading;
