import React, { useState, createContext } from "react";

export const FiltersContext = createContext();

export const FiltersProvider = (props) => {
  const [title, setTitle] = useState("");
  const [city, setCity] = useState(undefined);
  const [companyId, setCompanyId] = useState(undefined);
  const [investorIds, setInvestorIds] = useState(undefined);
  return (
    <FiltersContext.Provider
      value={{
        filteredTitle: [title, setTitle],
        selectedCity: [city, setCity],
        selectedCompanyId: [companyId, setCompanyId],
        selectedInvestorIds: [investorIds, setInvestorIds],
      }}
    >
      {props.children}
    </FiltersContext.Provider>
  );
};
