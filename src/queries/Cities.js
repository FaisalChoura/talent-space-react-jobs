import { gql } from "apollo-boost";

const QUERY_ALL_CITIES = gql`
  query AllCities {
    jobs(distinct_on: city) {
      city
    }
  }
`;

export { QUERY_ALL_CITIES };
