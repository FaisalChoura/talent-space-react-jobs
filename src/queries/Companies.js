import { gql } from "apollo-boost";

const QUERY_ALL_COMPANIES = gql`
  query AllCompanies {
    companies {
      id
      name
    }
  }
`;

export { QUERY_ALL_COMPANIES };
