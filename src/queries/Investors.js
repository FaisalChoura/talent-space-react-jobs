import { gql } from "apollo-boost";

const QUERY_ALL_INVESTORSS = gql`
  query AllInvestors {
    investors {
      id
      name
    }
  }
`;

export { QUERY_ALL_INVESTORSS };
