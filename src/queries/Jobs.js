import { gql } from "apollo-boost";

const QUERY_JOBS = gql`
  query fetchJobs {
    jobs {
      id
      title
      city
    }
  }
`;

const QUERY = gql`
  query MyQuery(
    $company_id: Int
    $title: String
    $investor_ids: [String!]
    $city_name: String
  ) {
    jobs(
      where: {
        title: { _like: $title }
        company_id: { _eq: $company_id }
        company: {
          company_investors: { investor: { name: { _in: $investor_ids } } }
        }
        city: { _ilike: $city_name }
      }
    ) {
      title
      city
      company {
        name
        company_investors {
          investor {
            name
          }
        }
      }
    }
  }
`;

export { QUERY_JOBS, QUERY };
